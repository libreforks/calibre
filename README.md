calibre
=========

<img align="left" src="resources/images/lt.png?raw=true"/>

This is a fully-libre fork of calibre, an e-book manager.
It can view, convert, edit and catalog e-books in all of the major e-book formats.
It can also talk to e-book reader devices. It can go out to the internet and fetch
metadata for your books.
It can download newspapers and convert them into e-books for convenient
reading. It is cross platform, running on GNU/Linux, Windows and OS X.

For more information, see the [calibre About page](https://calibre-ebook.com/about)

[![Build Status](https://api.travis-ci.org/kovidgoyal/calibre.svg)](https://travis-ci.org/kovidgoyal/calibre)
[![Build status](https://ci.appveyor.com/api/projects/status/v3nkfq0t3pse8lep?svg=true&passingText=windows%20OK&failingText=windows%20KO)](https://ci.appveyor.com/project/kovidgoyal/calibre)

<br>
<br>
<br>

Screenshots
-------------
[Screenshots page](https://calibre-ebook.com/demo)

Usage
-------
See the [User Manual](https://manual.calibre-ebook.com)

Development
-------------
[Setting up a development environment for calibre](https://manual.calibre-ebook.com/develop.html)


A [tarball of the source code](https://calibre-ebook.com/dist/src) for the 
current calibre release.

Bugs
------

Bug reports and feature requests should be made in the calibre bug tracker at [launchpad](https://bugs.launchpad.net/calibre).
If the bug is particular to this fork, then report it in the [Parabola Bug Tracker](https://labs.parabola.nu/projects)

